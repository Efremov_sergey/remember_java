package com.esv.rememberjava.ui.shopping_card;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.esv.rememberjava.adapter.MyShoppingCardRecyclerViewAdapter;
import com.esv.rememberjava.R;
import com.esv.rememberjava.adapter.ProductDiffUtil;
import com.esv.rememberjava.databinding.FragmentItemListBinding;

import org.jetbrains.annotations.NotNull;

public class ShoppingCardFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";

    private MyShoppingCardRecyclerViewAdapter mAdapter;

    public static ShoppingCardFragment newInstance(int columnCount) {
        ShoppingCardFragment fragment = new ShoppingCardFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentItemListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item_list, container, false);

        ShoppingCardViewModel mViewModel = new ShoppingCardViewModel(requireActivity().getApplication());
        mAdapter = new MyShoppingCardRecyclerViewAdapter(new ProductDiffUtil());
        binding.shoppingCard.setAdapter(mAdapter);
        binding.addProduct.setOnClickListener(v -> Navigation.findNavController(binding.getRoot()).navigate(R.id.action_shoppingCardFragment_to_addProductFragment));

        mViewModel.loadData().observe(getViewLifecycleOwner(), productPagingData -> mAdapter.submitData(getLifecycle(), productPagingData));

        return binding.getRoot();
    }
}