package com.esv.rememberjava.ui.product;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.esv.rememberjava.R;
import com.esv.rememberjava.databinding.AddProductFragmentBinding;

public class AddProductFragment extends Fragment {

    private AddProductViewModel mViewModel;

    public static AddProductFragment newInstance() {
        return new AddProductFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        AddProductFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.add_product_fragment, container, false);

        requireActivity().setTitle("Добавить продукт");

        mViewModel = new AddProductViewModel(requireActivity().getApplication());
        binding.setViewModel(mViewModel);
        //        binding.setViewModel(mViewModel);

//        mViewModel.onProductAdded.observe(getViewLifecycleOwner(), value -> {
//            // TODO: navigate pop
//        });

//        mViewModel.error.observe(getViewLifecycleOwner(), value -> {
//            Snackbar.make(binding.getRoot(), value, BaseTransientBottomBar.LENGTH_LONG).show();
//        });

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(AddProductViewModel.class);
        // TODO: Use the ViewModel
    }

}