package com.esv.rememberjava.ui.shopping_card;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;

import com.esv.rememberjava.entities.Product;
import com.esv.rememberjava.model.reposiory.ShoppingCardRepository;

import kotlinx.coroutines.CoroutineScope;

public class ShoppingCardViewModel extends AndroidViewModel {

    private final ShoppingCardRepository mRepository;

    public ShoppingCardViewModel(Application application) {
        super(application);
        mRepository = new ShoppingCardRepository(application);
        loadData();
    }

    public LiveData<PagingData<Product>> loadData() {
        CoroutineScope viewModelScope = ViewModelKt.getViewModelScope(this);
        return PagingLiveData.cachedIn(PagingLiveData.getLiveData(mRepository.loadFromDB()), viewModelScope);
    }
}