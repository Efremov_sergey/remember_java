package com.esv.rememberjava.ui.product;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.esv.rememberjava.entities.Product;
import com.esv.rememberjava.model.reposiory.ProductRepository;

public class AddProductViewModel extends AndroidViewModel {

    public MutableLiveData<String> name = new MutableLiveData<>();
    public MutableLiveData<Float> cost = new MutableLiveData<>();

    public MutableLiveData<Boolean> onProductAdded = new MutableLiveData<>(false);
    public MutableLiveData<String> error = new MutableLiveData<>("");

    private final ProductRepository mRepository;

    public AddProductViewModel(Application application) {
        super(application);
        mRepository = new ProductRepository(application);
    }

    public void onAddButtonClick() {
        if (name.getValue() != null && cost.getValue() != null) {
            mRepository.insertProduct(new Product(name.getValue(), cost.getValue()));
        }
    }
}