package com.esv.rememberjava.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.esv.rememberjava.entities.Product;

public class ProductDiffUtil extends DiffUtil.ItemCallback<Product> {
    @Override
    public boolean areItemsTheSame(@NonNull Product oldItem,
                                   @NonNull Product newItem) {
        // Id is unique.
        return oldItem.name.equals(newItem.name);
    }

    @Override
    public boolean areContentsTheSame(@NonNull Product oldItem,
                                      @NonNull Product newItem) {
        return oldItem.name.equals(newItem.name);
    }
}