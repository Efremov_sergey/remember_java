package com.esv.rememberjava.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.esv.rememberjava.R;
import com.esv.rememberjava.databinding.FragmentShoppingCardItemBinding;
import com.esv.rememberjava.entities.Product;

import org.jetbrains.annotations.NotNull;

public class MyShoppingCardRecyclerViewAdapter extends PagingDataAdapter<Product, MyShoppingCardRecyclerViewAdapter.ProductViewHolder> {

    public MyShoppingCardRecyclerViewAdapter(@NotNull DiffUtil.ItemCallback<Product> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @NotNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ProductViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ProductViewHolder holder, int position) {
        Product item = getItem(position);
        holder.bind(item);
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {

        FragmentShoppingCardItemBinding binding;

        ProductViewHolder(
                @NonNull ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_shopping_card_item, parent, false));

            binding = FragmentShoppingCardItemBinding.bind(itemView);
        }

        public void bind(Product product) {
            binding.content.setText(product.name);
            binding.price.setText(product.cost.toString());
        }
    }
}