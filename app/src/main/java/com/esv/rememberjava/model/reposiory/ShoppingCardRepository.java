package com.esv.rememberjava.model.reposiory;

import android.app.Application;

import androidx.paging.Pager;
import androidx.paging.PagingConfig;

import com.esv.rememberjava.dao.ProductDao;
import com.esv.rememberjava.database.AppDatabase;
import com.esv.rememberjava.entities.Product;

public class ShoppingCardRepository {

    private final ProductDao productDao;

    public ShoppingCardRepository(Application application) {
        AppDatabase db = AppDatabase.getInstance(application);
        productDao = db.productDao();
    }

    public Pager<Integer, Product> loadFromDB() {
        return new Pager(
                new PagingConfig(20),
                null,
                null,
                productDao::selectAll);
    }
}
