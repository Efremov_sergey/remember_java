package com.esv.rememberjava.entities;

import androidx.databinding.BaseObservable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity
public class Product {
    @PrimaryKey
    @NotNull
    public String name;
    public Float cost;

    public Product(@NotNull String name, Float cost) {
        this.name = name;
        this.cost = cost;
    }
}