package com.esv.rememberjava.model.reposiory;

import android.app.Application;

import com.esv.rememberjava.dao.ProductDao;
import com.esv.rememberjava.database.AppDatabase;
import com.esv.rememberjava.entities.Product;

public class ProductRepository {

    private final ProductDao productDao;

    public ProductRepository(Application application) {
        AppDatabase db = AppDatabase.getInstance(application);
        productDao = db.productDao();
    }

    public void insertProduct(Product product) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            productDao.insert(product);
        });
    }
}
