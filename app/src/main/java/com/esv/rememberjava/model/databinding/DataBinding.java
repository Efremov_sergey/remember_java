package com.esv.rememberjava.databinding;

import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;

public class DataBinding {
    @BindingAdapter("android:text")
    public static void setText(TextView view, Float value) {
        if (value == null)
            return;

        view.setText(String.valueOf(value));
    }

    @InverseBindingAdapter(attribute = "android:text", event = "android:textAttrChanged")
    public static Float getTextString(TextView view) {
        return Float.valueOf(view.getText().toString());
    }
}
